﻿using System;
using System.Data;
using System.Data.OleDb;
using System.IO;
using System.Xml;
using SautinSoft;

namespace CReader.BL
{
    public interface IFileManager
    {
        bool IsExist(string filePath);
        bool IsExistDirectory(string filePath);
        void ConvertPdfToXml(string folderPath);
        void ArrayPdf(string folderToPdf);
        DataTable XmlSerializer(string folderToXml);
        DataTable GetParseIncomingExcelFile(string filePath, string sheetName);
        DataTable GetVerificationResult(DataTable dtPdf, string filePath);
    }

    public class FileManager: IFileManager
    {
        //Проверка существования файла
        public bool IsExist(string filePath)
        {
            bool isExist = File.Exists(filePath);
            return isExist;
        }
        //Проверка существования папки
        public bool IsExistDirectory(string filePath)
        {
            bool isExistDirectory = Directory.Exists(filePath);
            return isExistDirectory;
        }
        //Конвертируем Pdf в XML
        public void ConvertPdfToXml(string pathToPdf)
        {
            //string pathToPdf = @"..\..\..\..\..\20783606.pdf";
            var pathToXml = Path.ChangeExtension(pathToPdf, ".xml");

            // Convert PDF file to XML file.
            PdfFocus f = new PdfFocus();

            // This property is necessary only for registered version.
            //f.Serial = "XXXXXXXXXXX";

            // Let's convert only tables to XML and skip all textual data.
            f.XmlOptions.ConvertNonTabularDataToSpreadsheet = false;

            f.OpenPdf(pathToPdf);

            if (f.PageCount > 0)
            {
                //var path2 = @"D:\\temp\\";
                //f.ToXml(path2 + Path.GetFileName(pathToXml));

                int result = f.ToXml(pathToXml);

                //Show HTML document in browser
                //if (result == 0)
                //{
                //    System.Diagnostics.Process.Start(pathToXml);
                //}
            }
        }
        //Cобираем коллекцию Pdf файлов
        public void ArrayPdf(string folderToPdf)
        {
            var arrayPdfs = Directory.GetFiles(folderToPdf, "*.PDF", SearchOption.AllDirectories);

            foreach (var arr in arrayPdfs)
            {
                ConvertPdfToXml(arr);
            }
        }
        //Возвращаем таблицу с данными из XML(Pdf) файлов
        public DataTable XmlSerializer(string folderToXml)
        {
            DataTable dtResult = new DataTable();

            dtResult.Columns.Add("Модель", typeof(string));
            dtResult.Columns.Add("VIN номер", typeof(string));
            dtResult.Columns.Add("Номер двигателя", typeof(string));
            dtResult.Columns.Add("КОММ-NR", typeof(string));
            dtResult.Columns.Add("Тип двигателя", typeof(string));
            dtResult.Columns.Add("Стоимость", typeof(decimal));
            dtResult.Columns.Add("Вес", typeof(string));
            dtResult.Columns.Add("Суммарная стоимость", typeof(decimal));

            var arrayXmls = Directory.GetFiles(folderToXml, "*.XML", SearchOption.AllDirectories);

            foreach (var arr in arrayXmls)
            {
                XmlDocument xmlDoc = new XmlDocument();
                xmlDoc.Load(arr);

                XmlNodeList xnlist = xmlDoc.SelectNodes("document/page/table/row");
                if (xnlist != null)
                {
                    var value = xnlist[5].InnerText;

                    DataRow dr = dtResult.NewRow();

                    int startWarrennumer = value.IndexOf("WARENNUMMER", StringComparison.Ordinal);
                    var warrennumer = value.Substring(startWarrennumer + 12, 100);
                    var warrennumerArray = warrennumer.Split(' ');

                    //model
                    dr[0] = warrennumerArray[0];
                    //vin
                    dr[1] = warrennumerArray[1];
                    //engineNumber
                    dr[2] = warrennumerArray[2] + ' ' + warrennumerArray[3];
                    //kommnr
                    dr[3] = warrennumerArray[4] + ' ' + warrennumerArray[5];
                    //engineType
                    dr[4] = warrennumerArray[6];
                    //price
                    dr[5] = decimal.Parse(warrennumerArray[7].Replace(".",""));

                    int startGewicht = value.IndexOf("GEWICHT ", StringComparison.Ordinal);
                    int endGewicht = value.IndexOf(" KG", StringComparison.Ordinal);
                    //weigth
                    dr[6] = value.Substring(startGewicht + 8, endGewicht - (startGewicht + 8)).Replace(".","");
                    //dr[6] = value.Substring(startGewicht + 8, endGewicht - (startGewicht + 8));

                    int startValueSummeRechnung = value.IndexOf("RECHNUNG ", StringComparison.Ordinal);
                    var summeRechnung = value.Substring(startValueSummeRechnung + 9, 20);
                    var summeRechnungArray = summeRechnung.Split(' ');
                    //total price
                    dr[7] = decimal.Parse(summeRechnungArray[0].Replace(".",""));
                    
                    dtResult.Rows.Add(dr);
                    dtResult.AcceptChanges();
                }
            }

            return dtResult;
        }
        //Возвращаем таблицу с данными из Excel файла
        public DataTable GetParseIncomingExcelFile(string filePath, string sheetName)
        {
            string sqlQuery = string.Format("SELECT [F1],[F2],[F3],[F4],[F5],[F6],[F7],[F8],[F9],[F10],[F11],[F12],[F13],[F14],[F15],[F16],[F17],[F18] FROM [{0}$]",sheetName);

            string connString = string.Format("Provider = Microsoft.ACE.OLEDB.12.0; Data Source = '{0}';Extended Properties =\"Excel 12.0;HDR=NO;IMEX=1\"", filePath);

            DataSet ds = new DataSet();

            OleDbConnection con = new OleDbConnection(connString);
            OleDbDataAdapter da = new OleDbDataAdapter(sqlQuery, con);
            da.Fill(ds);

            DataTable dt = ds.Tables[0];
            DataTable dtResult = new DataTable();

            dtResult.Columns.Add("№", typeof(int));
            dtResult.Columns.Add("Invoice", typeof(string));
            dtResult.Columns.Add("N счета, дата счета", typeof(string));
            dtResult.Columns.Add("VIN", typeof(string));
            dtResult.Columns.Add("Марка Модель", typeof(string));
            dtResult.Columns.Add("Код ТНВЭД Customs Code", typeof(string));
            dtResult.Columns.Add("Вес кг, Weight", typeof(string));
            dtResult.Columns.Add("Дилер, Dealer", typeof(string));
            dtResult.Columns.Add("Статус,Status", typeof(string));
            dtResult.Columns.Add("Номер двигателя", typeof(string));
            dtResult.Columns.Add("Тип ДВС", typeof(string));
            dtResult.Columns.Add("Объем", typeof(string));
            dtResult.Columns.Add("Мощ. кВт", typeof(string));
            dtResult.Columns.Add("Мощ. ЛС", typeof(string));
            dtResult.Columns.Add("Дата производства", typeof(DateTime));
            dtResult.Columns.Add("Изготовитель", typeof(string));
            dtResult.Columns.Add("Цвет", typeof(string));
            dtResult.Columns.Add("Стоимость", typeof(decimal));
            dtResult.Columns.Add("Полная масса", typeof(string));

            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i][0].ToString().Equals("N"))
                {
                    int indexRow = dt.Rows.IndexOf(dt.Rows[i]) + 1;
                    int j = 1;
                    string invoiceNumber = string.Empty;

                    while (!dt.Rows[indexRow][0].ToString().Equals(string.Empty))
                    {
                        DataRow dr = dtResult.NewRow();

                        dr[0] = j++;

                        if (dt.Rows[indexRow][1].ToString().Contains("STP"))
                        {
                            invoiceNumber = dt.Rows[indexRow][1].ToString();
                            indexRow++;
                        }
                        //STP invoice
                        dr[1] = invoiceNumber;
                        //N invoice, date
                        dr[2] = dt.Rows[indexRow][1].ToString();
                        //Vin
                        dr[3] = dt.Rows[indexRow][2].ToString();
                        //Mark model
                        dr[4] = dt.Rows[indexRow][3].ToString();
                        //Customs Code
                        dr[5] = dt.Rows[indexRow][4].ToString();
                        //Weight
                        dr[6] = dt.Rows[indexRow][5].ToString();
                        //Dealer
                        dr[7] = dt.Rows[indexRow][6].ToString();
                        //Status
                        dr[8] = dt.Rows[indexRow][7].ToString();
                        //Engine number
                        dr[9] = dt.Rows[indexRow][8].ToString();
                        //DVS type
                        dr[10] = dt.Rows[indexRow][9].ToString();
                        //Volume
                        dr[11] = dt.Rows[indexRow][10].ToString();
                        //Capacity, kVt
                        dr[12] = dt.Rows[indexRow][11].ToString();
                        //Capacity, HP
                        dr[13] = dt.Rows[indexRow][12].ToString();
                        //Date of manufacture
                        dr[14] = DateTime.Parse(dt.Rows[indexRow][13].ToString()).ToShortDateString();
                        //Manufacturer
                        dr[15] = dt.Rows[indexRow][14].ToString();
                        //Color
                        dr[16] = dt.Rows[indexRow][15].ToString();
                        //Price
                        dr[17] = decimal.Parse(dt.Rows[indexRow][16].ToString());
                        //Full mass
                        dr[18] = dt.Rows[indexRow][17].ToString();

                        dtResult.Rows.Add(dr);
                        dtResult.AcceptChanges();
                        indexRow++;
                    }
                }
            }

            return dtResult;
        }
        public DataTable GetVerificationResult(DataTable dtPdf, string filePath)
        {
            DataTable dtExcel = GetParseIncomingExcelFile(filePath, "sheet1");
            DataTable dtResult = new DataTable();

            dtResult.Columns.Add("№", typeof(int));
            dtResult.Columns.Add("Invoice", typeof(string));
            dtResult.Columns.Add("N счета, дата счета", typeof(string));
            dtResult.Columns.Add("VIN", typeof(string));
            dtResult.Columns.Add("PDF/VIN", typeof(string));
            dtResult.Columns.Add("Вес кг, Weight", typeof(string));
            dtResult.Columns.Add("PDF/WEIGHT", typeof(string));
            dtResult.Columns.Add("Номер двигателя", typeof(string));
            dtResult.Columns.Add("PDF/Номер двигателя", typeof(string));
            dtResult.Columns.Add("Стоимость", typeof(decimal));
            dtResult.Columns.Add("PDF/Стоимость", typeof(decimal));
            dtResult.Columns.Add("Статус", typeof(string));

            int i = 1;
            bool noVin = false;

            foreach (var rowExcel in dtExcel.AsEnumerable())
            {
                DataRow dr = dtResult.NewRow();

                foreach (var rowPdf in dtPdf.AsEnumerable())
                {
                    if (rowExcel[3].ToString().Equals(rowPdf[1].ToString()))
                    {
                        //if (!rowExcel[6].Equals(rowPdf[6]) || !rowExcel[9].Equals(rowPdf[2]) ||
                        //    !rowExcel[17].Equals(rowPdf[7]))
                        //{
                        //    dr[0] = i;
                        //    dr[1] = rowExcel[1].ToString();
                        //    dr[2] = rowExcel[2].ToString();
                        //    dr[3] = rowExcel[3].ToString();
                        //    dr[4] = rowPdf[1].ToString();
                        //    dr[5] = rowExcel[6].ToString();
                        //    dr[6] = rowPdf[6].ToString();
                        //    dr[7] = rowExcel[9].ToString();
                        //    dr[8] = rowPdf[2].ToString();
                        //    dr[9] = rowExcel[17].ToString();
                        //    dr[10] = rowPdf[7].ToString();

                        //    dtResult.Rows.Add(dr);
                        //    dtResult.AcceptChanges();
                        //    i++;
                        //}
                        string status = string.Empty;

                        if (!rowExcel[6].Equals(rowPdf[6]))
                        {
                            status = "Вес,";
                        }else if (!rowExcel[9].Equals(rowPdf[2]))
                        {
                            status += "Номер двигателя,";
                        }else if (!rowExcel[17].Equals(rowPdf[7]))
                        {
                            status += "Стоимость,";
                        }

                        if (status.Length > 0)
                        {
                            dr[0] = i;
                            dr[1] = rowExcel[1].ToString();
                            dr[2] = rowExcel[2].ToString();
                            dr[3] = rowExcel[3].ToString();
                            dr[4] = rowPdf[1].ToString();
                            dr[5] = rowExcel[6].ToString();
                            dr[6] = rowPdf[6].ToString();
                            dr[7] = rowExcel[9].ToString();
                            dr[8] = rowPdf[2].ToString();
                            dr[9] = rowExcel[17].ToString();
                            dr[10] = rowPdf[7].ToString();
                            dr[11] = @"Проверить: " + status.Remove(status.Length - 1, 1);

                            dtResult.Rows.Add(dr);
                            dtResult.AcceptChanges();
                            i++;
                        }

                        noVin = false;
                        break;
                    }

                    noVin = true;
                }
                if (noVin)
                {
                    dr[0] = i;
                    dr[1] = rowExcel[1].ToString();
                    dr[2] = rowExcel[2].ToString();
                    dr[3] = rowExcel[3].ToString();
                    dr[5] = rowExcel[6].ToString();
                    dr[7] = rowExcel[9].ToString();
                    dr[9] = rowExcel[17].ToString();
                    dr[11] = @"Нет PDF Инвойса";

                    dtResult.Rows.Add(dr);
                    dtResult.AcceptChanges();
                    i++;
                }
            }

            return dtResult;
        }
    }
}