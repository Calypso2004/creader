﻿namespace CReader
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.lblPathPDF = new System.Windows.Forms.Label();
            this.txtPathPDF = new System.Windows.Forms.TextBox();
            this.btnSelectFolderPDF = new System.Windows.Forms.Button();
            this.lblFileExcel = new System.Windows.Forms.Label();
            this.txtPathFileExcel = new System.Windows.Forms.TextBox();
            this.btnSelectFileExcel = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.dgvDataSource = new System.Windows.Forms.DataGridView();
            this.contextMenuDataGridView = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.ExportToExcelToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.lblVersion = new System.Windows.Forms.Label();
            this.btnGetDataSource = new System.Windows.Forms.Button();
            this.btnCheckFileExcel = new System.Windows.Forms.Button();
            this.btnVerification = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsslblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.lblStatus = new System.Windows.Forms.ToolStripStatusLabel();
            this.bgwCheckFileExcel = new System.ComponentModel.BackgroundWorker();
            this.bgwVerification = new System.ComponentModel.BackgroundWorker();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDataSource)).BeginInit();
            this.contextMenuDataGridView.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblPathPDF
            // 
            this.lblPathPDF.AutoSize = true;
            this.lblPathPDF.Location = new System.Drawing.Point(12, 43);
            this.lblPathPDF.Name = "lblPathPDF";
            this.lblPathPDF.Size = new System.Drawing.Size(67, 13);
            this.lblPathPDF.TabIndex = 0;
            this.lblPathPDF.Text = "Путь к PDF:";
            // 
            // txtPathPDF
            // 
            this.txtPathPDF.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPathPDF.Enabled = false;
            this.txtPathPDF.Location = new System.Drawing.Point(85, 42);
            this.txtPathPDF.Name = "txtPathPDF";
            this.txtPathPDF.Size = new System.Drawing.Size(598, 20);
            this.txtPathPDF.TabIndex = 1;
            // 
            // btnSelectFolderPDF
            // 
            this.btnSelectFolderPDF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectFolderPDF.Location = new System.Drawing.Point(689, 40);
            this.btnSelectFolderPDF.Name = "btnSelectFolderPDF";
            this.btnSelectFolderPDF.Size = new System.Drawing.Size(75, 23);
            this.btnSelectFolderPDF.TabIndex = 2;
            this.btnSelectFolderPDF.Text = "Выбрать";
            this.btnSelectFolderPDF.UseVisualStyleBackColor = true;
            // 
            // lblFileExcel
            // 
            this.lblFileExcel.AutoSize = true;
            this.lblFileExcel.Location = new System.Drawing.Point(12, 81);
            this.lblFileExcel.Name = "lblFileExcel";
            this.lblFileExcel.Size = new System.Drawing.Size(68, 13);
            this.lblFileExcel.TabIndex = 3;
            this.lblFileExcel.Text = "Файл Excel:";
            // 
            // txtPathFileExcel
            // 
            this.txtPathFileExcel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtPathFileExcel.Enabled = false;
            this.txtPathFileExcel.Location = new System.Drawing.Point(86, 77);
            this.txtPathFileExcel.Name = "txtPathFileExcel";
            this.txtPathFileExcel.Size = new System.Drawing.Size(597, 20);
            this.txtPathFileExcel.TabIndex = 4;
            // 
            // btnSelectFileExcel
            // 
            this.btnSelectFileExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectFileExcel.Location = new System.Drawing.Point(689, 75);
            this.btnSelectFileExcel.Name = "btnSelectFileExcel";
            this.btnSelectFileExcel.Size = new System.Drawing.Size(75, 23);
            this.btnSelectFileExcel.TabIndex = 5;
            this.btnSelectFileExcel.Text = "Выбрать";
            this.btnSelectFileExcel.UseVisualStyleBackColor = true;
            // 
            // dgvDataSource
            // 
            this.dgvDataSource.AllowUserToAddRows = false;
            this.dgvDataSource.AllowUserToDeleteRows = false;
            this.dgvDataSource.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvDataSource.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDataSource.ContextMenuStrip = this.contextMenuDataGridView;
            this.dgvDataSource.Location = new System.Drawing.Point(12, 105);
            this.dgvDataSource.Name = "dgvDataSource";
            this.dgvDataSource.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect;
            this.dgvDataSource.Size = new System.Drawing.Size(924, 459);
            this.dgvDataSource.TabIndex = 6;
            // 
            // contextMenuDataGridView
            // 
            this.contextMenuDataGridView.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ExportToExcelToolStripMenuItem});
            this.contextMenuDataGridView.Name = "contextMenuDataGridView";
            this.contextMenuDataGridView.Size = new System.Drawing.Size(157, 26);
            // 
            // ExportToExcelToolStripMenuItem
            // 
            this.ExportToExcelToolStripMenuItem.Image = global::CReader.Properties.Resources.Excel;
            this.ExportToExcelToolStripMenuItem.Name = "ExportToExcelToolStripMenuItem";
            this.ExportToExcelToolStripMenuItem.ShowShortcutKeys = false;
            this.ExportToExcelToolStripMenuItem.Size = new System.Drawing.Size(156, 22);
            this.ExportToExcelToolStripMenuItem.Text = "Выгрузка в Excel";
            this.ExportToExcelToolStripMenuItem.Click += new System.EventHandler(this.ExportToExcelToolStripMenuItem_Click);
            // 
            // lblVersion
            // 
            this.lblVersion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.lblVersion.AutoSize = true;
            this.lblVersion.Location = new System.Drawing.Point(875, 9);
            this.lblVersion.Name = "lblVersion";
            this.lblVersion.Size = new System.Drawing.Size(61, 13);
            this.lblVersion.TabIndex = 7;
            this.lblVersion.Text = "ver. 0.0.0.0";
            // 
            // btnGetDataSource
            // 
            this.btnGetDataSource.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetDataSource.Location = new System.Drawing.Point(770, 40);
            this.btnGetDataSource.Name = "btnGetDataSource";
            this.btnGetDataSource.Size = new System.Drawing.Size(75, 23);
            this.btnGetDataSource.TabIndex = 8;
            this.btnGetDataSource.Text = "Проверить";
            this.btnGetDataSource.UseVisualStyleBackColor = true;
            // 
            // btnCheckFileExcel
            // 
            this.btnCheckFileExcel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnCheckFileExcel.Location = new System.Drawing.Point(770, 75);
            this.btnCheckFileExcel.Name = "btnCheckFileExcel";
            this.btnCheckFileExcel.Size = new System.Drawing.Size(75, 23);
            this.btnCheckFileExcel.TabIndex = 9;
            this.btnCheckFileExcel.Text = "Проверить";
            this.btnCheckFileExcel.UseVisualStyleBackColor = true;
            // 
            // btnVerification
            // 
            this.btnVerification.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnVerification.Location = new System.Drawing.Point(861, 38);
            this.btnVerification.Name = "btnVerification";
            this.btnVerification.Size = new System.Drawing.Size(75, 61);
            this.btnVerification.TabIndex = 10;
            this.btnVerification.Text = "Сверка данных";
            this.btnVerification.UseVisualStyleBackColor = true;
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.WorkerReportsProgress = true;
            this.backgroundWorker1.WorkerSupportsCancellation = true;
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsslblStatus,
            this.lblStatus});
            this.statusStrip1.Location = new System.Drawing.Point(0, 567);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(948, 22);
            this.statusStrip1.TabIndex = 11;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsslblStatus
            // 
            this.tsslblStatus.Name = "tsslblStatus";
            this.tsslblStatus.Size = new System.Drawing.Size(46, 17);
            this.tsslblStatus.Text = "Статус:";
            // 
            // lblStatus
            // 
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(0, 17);
            // 
            // bgwCheckFileExcel
            // 
            this.bgwCheckFileExcel.WorkerReportsProgress = true;
            this.bgwCheckFileExcel.WorkerSupportsCancellation = true;
            // 
            // bgwVerification
            // 
            this.bgwVerification.WorkerReportsProgress = true;
            this.bgwVerification.WorkerSupportsCancellation = true;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(948, 589);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.btnVerification);
            this.Controls.Add(this.btnCheckFileExcel);
            this.Controls.Add(this.btnGetDataSource);
            this.Controls.Add(this.btnSelectFolderPDF);
            this.Controls.Add(this.lblVersion);
            this.Controls.Add(this.dgvDataSource);
            this.Controls.Add(this.btnSelectFileExcel);
            this.Controls.Add(this.txtPathFileExcel);
            this.Controls.Add(this.lblFileExcel);
            this.Controls.Add(this.txtPathPDF);
            this.Controls.Add(this.lblPathPDF);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            this.Text = "Launch CReader";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDataSource)).EndInit();
            this.contextMenuDataGridView.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblPathPDF;
        private System.Windows.Forms.TextBox txtPathPDF;
        private System.Windows.Forms.Button btnSelectFolderPDF;
        private System.Windows.Forms.Label lblFileExcel;
        private System.Windows.Forms.TextBox txtPathFileExcel;
        private System.Windows.Forms.Button btnSelectFileExcel;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.DataGridView dgvDataSource;
        private System.Windows.Forms.Label lblVersion;
        private System.Windows.Forms.Button btnGetDataSource;
        private System.Windows.Forms.Button btnCheckFileExcel;
        private System.Windows.Forms.Button btnVerification;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsslblStatus;
        private System.Windows.Forms.ToolStripStatusLabel lblStatus;
        private System.ComponentModel.BackgroundWorker bgwCheckFileExcel;
        private System.ComponentModel.BackgroundWorker bgwVerification;
        private System.Windows.Forms.ContextMenuStrip contextMenuDataGridView;
        private System.Windows.Forms.ToolStripMenuItem ExportToExcelToolStripMenuItem;
    }
}

