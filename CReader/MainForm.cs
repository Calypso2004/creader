﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Deployment.Application;
using System.Linq;
using System.Windows.Forms;

namespace CReader
{
    public interface IMainForm
    {
        string FolderPathPdf { get; set; }
        string FilePathExcel { get; set; }
        string StatusText { get; set; }

        bool IsBusy { get; }
        bool BgwCheckFileExcelIsBusy { get; }
        bool BgwVerificationIsBusy { get; }
        void RunWorkerAsync();
        void BgwCheckFileExcelRunWorkerAsync();
        void BgwVerificationRunWorkerAsunc();
        void BgwCheckFileExcelReportProgress(int i);

        DataGridView DataGridViewDataSource { get; set; }

        event EventHandler FolderSelectPdfClick;
        event EventHandler GetDataSourceClick;

        event EventHandler FileSelectExcelClick;
        event EventHandler CheckFileExcelClick;

        event EventHandler VerificationClick;

        event DoWorkEventHandler BgwDoWork;
        event ProgressChangedEventHandler BgwProgressChanged;
        event RunWorkerCompletedEventHandler BgwRunWorkerCompleted;

        event DoWorkEventHandler BgwDoWorkCheckFileExcel;
        event ProgressChangedEventHandler BgwProgressChangedCheckFileExcel;
        event RunWorkerCompletedEventHandler BgwRunWorkerCompletedCheckFileExcel;

        event DoWorkEventHandler BgwDoWorkVerification;
        event ProgressChangedEventHandler BgwProgressChangedVerification;
        event RunWorkerCompletedEventHandler BgwRunWorkerCompletedVerification;
    }

    public partial class MainForm : Form, IMainForm
    {
        public MainForm()
        {
            InitializeComponent();

            btnSelectFolderPDF.Click += BtnSelectFolderPDF_Click;
            btnGetDataSource.Click += BtnGetDataSource_Click;

            btnSelectFileExcel.Click += BtnSelectFileExcel_Click;
            btnCheckFileExcel.Click += BtnCheckFileExcel_Click;

            btnVerification.Click += BtnVerification_Click;

            backgroundWorker1.DoWork += BackgroundWorker1_DoWork;
            backgroundWorker1.ProgressChanged += BackgroundWorker1_ProgressChanged;
            backgroundWorker1.RunWorkerCompleted += BackgroundWorker1_RunWorkerCompleted;

            bgwCheckFileExcel.DoWork += BgwCheckFileExcel_DoWork;
            bgwCheckFileExcel.ProgressChanged += BgwCheckFileExcel_ProgressChanged;
            bgwCheckFileExcel.RunWorkerCompleted += BgwCheckFileExcel_RunWorkerCompleted;

            bgwVerification.DoWork += BgwVerification_DoWork;
            bgwVerification.ProgressChanged += BgwVerification_ProgressChanged;
            bgwVerification.RunWorkerCompleted += BgwVerification_RunWorkerCompleted;
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            // версия компиляции
            Version ver = ApplicationDeployment.CurrentDeployment.CurrentVersion;
            lblVersion.Text = @"ver. " + ver;
        }

        #region IMainForm

        public string FolderPathPdf
        {
            get { return txtPathPDF.Text; }
            set { txtPathPDF.Text = value; }
        }

        public string FilePathExcel
        {
            get { return txtPathFileExcel.Text; }
            set { txtPathFileExcel.Text = value; }
        }

        public DataGridView DataGridViewDataSource
        {
            get { return dgvDataSource; }
            set { dgvDataSource.DataSource = value; }
        }

        public string StatusText
        {
            get { return lblStatus.Text; }
            set { lblStatus.Text = value; }
        }

        public bool IsBusy
        {
            get { return backgroundWorker1.IsBusy; }
        }

        public bool BgwCheckFileExcelIsBusy
        {
            get { return bgwCheckFileExcel.IsBusy; }
        }

        public bool BgwVerificationIsBusy
        {
            get { return bgwVerification.IsBusy; }
        }

        public void RunWorkerAsync()
        {
            backgroundWorker1.RunWorkerAsync();
        }

        public void BgwCheckFileExcelRunWorkerAsync()
        {
            bgwCheckFileExcel.RunWorkerAsync();
        }

        public void BgwVerificationRunWorkerAsunc()
        {
            bgwVerification.RunWorkerAsync();
        }

        public void BgwCheckFileExcelReportProgress(int i)
        {
            bgwCheckFileExcel.ReportProgress(i);
        }

        public event EventHandler FolderSelectPdfClick;
        public event EventHandler GetDataSourceClick;

        public event EventHandler FileSelectExcelClick;
        public event EventHandler CheckFileExcelClick;

        public event EventHandler VerificationClick;

        public event DoWorkEventHandler BgwDoWork;
        public event ProgressChangedEventHandler BgwProgressChanged;
        public event RunWorkerCompletedEventHandler BgwRunWorkerCompleted;

        public event DoWorkEventHandler BgwDoWorkCheckFileExcel;
        public event ProgressChangedEventHandler BgwProgressChangedCheckFileExcel;
        public event RunWorkerCompletedEventHandler BgwRunWorkerCompletedCheckFileExcel;

        public event DoWorkEventHandler BgwDoWorkVerification;
        public event ProgressChangedEventHandler BgwProgressChangedVerification;
        public event RunWorkerCompletedEventHandler BgwRunWorkerCompletedVerification;

        #endregion

        #region Проброс событий

        private void BtnSelectFolderPDF_Click(object sender, EventArgs e)
        {
            FolderSelectPdfClick?.Invoke(this, EventArgs.Empty);
        }

        private void BtnSelectFileExcel_Click(object sender, EventArgs e)
        {
            FileSelectExcelClick?.Invoke(this, EventArgs.Empty);
        }

        private void BtnGetDataSource_Click(object sender, EventArgs e)
        {
            GetDataSourceClick?.Invoke(this, EventArgs.Empty);
        }

        private void BtnCheckFileExcel_Click(object sender, EventArgs e)
        {
            CheckFileExcelClick?.Invoke(this, EventArgs.Empty);
        }

        private void BtnVerification_Click(object sender, EventArgs e)
        {
            VerificationClick?.Invoke(this, EventArgs.Empty);
        }

        private void BackgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BgwRunWorkerCompleted?.Invoke(this, e);
        }

        private void BackgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            BgwProgressChanged?.Invoke(this, e);
        }

        private void BackgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            BgwDoWork?.Invoke(this, e);
        }

        private void BgwCheckFileExcel_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BgwRunWorkerCompletedCheckFileExcel?.Invoke(this, e);
        }

        private void BgwCheckFileExcel_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            BgwProgressChangedCheckFileExcel?.Invoke(this, e);
        }

        private void BgwCheckFileExcel_DoWork(object sender, DoWorkEventArgs e)
        {
            BgwDoWorkCheckFileExcel?.Invoke(this, e);
        }

        private void BgwVerification_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            BgwRunWorkerCompletedVerification?.Invoke(this, e);
        }

        private void BgwVerification_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            BgwProgressChangedVerification?.Invoke(this, e);
        }

        private void BgwVerification_DoWork(object sender, DoWorkEventArgs e)
        {
            BgwDoWorkVerification?.Invoke(this, e);
        }

        #endregion

        private void ExportToExcelToolStripMenuItem_Click(object sender, EventArgs e)
        {
            #region TEST

            //var ordered = dgvDataSource.SelectedCells.OrderBy(c => c.Index);
            //int selectedCellCount = dgvDataSource.GetCellCount(DataGridViewElementStates.Selected);

            //if (selectedCellCount > 0)
            //{
            //    if (dgvDataSource.AreAllCellsSelected(true))
            //    {
            //        MessageBox.Show("All cells are selected", "Selected Cells");
            //    }
            //    else
            //    {
            //        System.Text.StringBuilder sb =
            //    new System.Text.StringBuilder();

            //        for (int i = 0;
            //            i < selectedCellCount; i++)
            //        {

            //            dgvDataSource.SelectedCells[i].

            //        }

            //        sb.Append("Total: " + selectedCellCount.ToString());
            //        MessageBox.Show(sb.ToString(), "Selected Cells");
            //    }
            //}

            //DataTable dtResult = new DataTable();
            //DataRow dr = null;
            //int columnIndex = 0;
            //int rowIndex = -1;
            //int i = 0;

            //for (int counter = 0; counter < dgvDataSource.SelectedCells.Count; counter++)
            //{
            //    if (!dtResult.Columns.Contains(dgvDataSource.SelectedCells[counter].OwningColumn.DataPropertyName))
            //    {
            //        dtResult.Columns.Add(dgvDataSource.SelectedCells[counter].OwningColumn.DataPropertyName, dgvDataSource.SelectedCells[counter].FormattedValueType);
            //    }
            //}

            //int countRows = dgvDataSource.SelectedCells.Count/dtResult.Columns.Count;

            //for (int j = 0; j < countRows; j++)
            //{
            //    dr = dtResult.NewRow();

            //    for (int k = 0; k < dtResult.Columns.Count; k++)
            //    {
            //        for (int l = 0; l < dgvDataSource.SelectedCells.Count; l++)
            //        {
            //            dr[k] = dgvDataSource.SelectedCells[l].EditedFormattedValue;
            //        }
            //    }

            //    dtResult.Rows.Add(dr);
            //    dtResult.AcceptChanges();
            //}

            ////for (int counter = 0; counter < dgvDataSource.SelectedCells.Count; counter++)
            ////{


            ////    if (!dgvDataSource.SelectedCells[counter].RowIndex.Equals(rowIndex))
            ////    {
            ////        dr = dtResult.NewRow();
            ////        rowIndex = dgvDataSource.SelectedCells[counter].RowIndex;

            ////        dtResult.Rows.Add(dr);
            ////        dtResult.AcceptChanges();
            ////    }
            ////}

            //dgvDataSource.DataSource = dtResult;

            #endregion
        }
    }
}
