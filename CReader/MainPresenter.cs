﻿using CReader.BL;
using System;
using System.ComponentModel;
using System.Threading;
using System.Windows.Forms;

namespace CReader
{
    public class MainPresenter
    {
        private readonly IMainForm _view;
        private readonly IFileManager _manager;
        private readonly IMessageService _messageService;

        public MainPresenter(IMainForm view, IFileManager manager, IMessageService service)
        {
            _view = view;
            _manager = manager;
            _messageService = service;

            _view.FolderPathPdf = Properties.Settings.Default.FolderPathPDF;
            _view.FilePathExcel = Properties.Settings.Default.FilePathEXCEL;

            _view.FolderSelectPdfClick += _view_FolderSelectPdfClick;
            _view.GetDataSourceClick += _view_GetDataSourceClick;

            _view.FileSelectExcelClick += _view_FileSelectExcelClick;
            _view.CheckFileExcelClick += _view_CheckFileExcelClick;

            _view.VerificationClick += _view_VerificationClick;

            _view.BgwDoWork += _view_BgwDoWork;
            _view.BgwProgressChanged += _view_BgwProgressChanged;
            _view.BgwRunWorkerCompleted += _view_BgwRunWorkerCompleted;

            _view.BgwDoWorkCheckFileExcel += _view_BgwDoWorkCheckFileExcel;
            _view.BgwProgressChangedCheckFileExcel += _view_BgwProgressChangedCheckFileExcel;
            _view.BgwRunWorkerCompletedCheckFileExcel += _view_BgwRunWorkerCompletedCheckFileExcel;

            _view.BgwDoWorkVerification += _view_BgwDoWorkVerification;
            _view.BgwProgressChangedVerification += _view_BgwProgressChangedVerification;
            _view.BgwRunWorkerCompletedVerification += _view_BgwRunWorkerCompletedVerification;
        }

        private void _view_BgwDoWorkVerification(object sender, DoWorkEventArgs e)
        {
            try
            {
                var isExist1 = _manager.IsExistDirectory(_view.FolderPathPdf);
                var isExist2 = _manager.IsExist(_view.FilePathExcel);
                if (!isExist1)
                {
                    _messageService.ShowExclamation("Выбранный путь не существует");
                    return;
                }
                if (!isExist2)
                {
                    _messageService.ShowExclamation("Выбранный файл не существует");
                    return;
                }

                _view.StatusText = @"In Progress...";

                //_manager.ArrayPdf(_view.FolderPathPdf);

                e.Result = _manager.GetVerificationResult(_manager.XmlSerializer(_view.FolderPathPdf), _view.FilePathExcel);
            }
            catch (Exception ex)
            {
                _messageService.ShowError(ex.Message);
            }
        }

        private void _view_BgwProgressChangedVerification(object sender, ProgressChangedEventArgs e)
        {
            throw new NotImplementedException();
        }

        private void _view_BgwRunWorkerCompletedVerification(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                _view.DataGridViewDataSource.DataSource = e.Result;
                _view.DataGridViewDataSource.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

                _view.StatusText = @"Completed";
            }
            catch (Exception ex)
            {
                _messageService.ShowError(ex.Message);
            }
        }
        private void _view_VerificationClick(object sender, EventArgs e)
        {
            if (!_view.BgwVerificationIsBusy)
            {
                _view.BgwVerificationRunWorkerAsunc();
            }
            else
            {
                _view.StatusText = @"Busy processing, please wait";
            }
        }

        private void _view_BgwDoWorkCheckFileExcel(object sender, DoWorkEventArgs e)
        {
            var isExist = _manager.IsExist(_view.FilePathExcel);
            if (!isExist)
            {
                _messageService.ShowExclamation("Выбранный файл не существует");
            }

            _view.StatusText = @"In Progress...";

            //for (int i = 0; i < 100; i++)
            //{
            //    Thread.Sleep(100);
            //    _view.BgwCheckFileExcelReportProgress(i);                
            //}

            e.Result = _manager.GetParseIncomingExcelFile(_view.FilePathExcel,
                    "sheet1");
        }

        private void _view_BgwProgressChangedCheckFileExcel(object sender, ProgressChangedEventArgs e)
        {
            _view.StatusText = e.ProgressPercentage + @"%";
        }

        private void _view_BgwRunWorkerCompletedCheckFileExcel(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                _view.DataGridViewDataSource.DataSource = e.Result;
                _view.DataGridViewDataSource.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

                _view.StatusText = @"Completed";
            }
            catch (Exception ex)
            {
                _messageService.ShowError(ex.Message);
            }
        }

        //DoWork - отдельного потока
        private void _view_BgwDoWork(object sender, DoWorkEventArgs e)
        {
            var isExist1 = _manager.IsExistDirectory(_view.FolderPathPdf);
            var isExist2 = _manager.IsExist(_view.FilePathExcel);
            if (!isExist1)
            {
                _messageService.ShowExclamation("Выбранный путь не существует");
                return;
            }
            if (!isExist2)
            {
                _messageService.ShowExclamation("Выбранный файл не существует");
                return;
            }

            _view.StatusText = @"In Progress...";

            _manager.ArrayPdf(_view.FolderPathPdf);

            e.Result = _manager.XmlSerializer(_view.FolderPathPdf);
        }

        //ProgressChanged - отлельного потока
        private void _view_BgwProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            _view.StatusText = e.ProgressPercentage + @"%";
        }

        //Complete - отдельного потока
        private void _view_BgwRunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            try
            {
                _view.DataGridViewDataSource.DataSource = e.Result;
                _view.DataGridViewDataSource.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

                _view.StatusText = @"Completed";
            }
            catch (Exception ex)
            {
                _messageService.ShowError(ex.Message);
            }

        }

        private void _view_CheckFileExcelClick(object sender, EventArgs e)
        {
            if (!_view.BgwCheckFileExcelIsBusy)
            {
                _view.BgwCheckFileExcelRunWorkerAsync();
            }
            else
            {
                _view.StatusText = @"Busy processing, please wait";
            }

            #region OldVariant
            //try
            //{
            //    bool isExist = _manager.IsExist(_view.FilePathExcel);
            //    if (!isExist)
            //    {
            //        _messageService.ShowExclamation("Выбранный файл не существует");
            //        return;
            //    }
            //    _view.DataGridViewDataSource.DataSource = _manager.GetParseIncomingExcelFile(_view.FilePathExcel, "sheet1");
            //    _view.DataGridViewDataSource.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;
            //}
            //catch (Exception ex)
            //{
            //    _messageService.ShowError(ex.Message);
            //}
            #endregion

        }

        private void _view_GetDataSourceClick(object sender, EventArgs e)
        {
            if (!_view.IsBusy)
            {
                _view.RunWorkerAsync();
            }
            else
            {
                _view.StatusText = @"Busy processing, please wait";
            }

            #region OldVariant

            //try
            //{
            //    bool isExist1 = _manager.IsExistDirectory(_view.FolderPathPdf);
            //    bool isExist2 = _manager.IsExist(_view.FilePathExcel);
            //    if (!isExist1)
            //    {
            //        _messageService.ShowExclamation("Выбранный путь не существует");
            //        return;
            //    }
            //    else if (!isExist2)
            //    {
            //        _messageService.ShowExclamation("Выбранный файл не существует");
            //        return;
            //    }

            //    _manager.ArrayPdf(_view.FolderPathPdf);

            //    _view.DataGridViewDataSource.DataSource = _manager.XmlSerializer(_view.FolderPathPdf);
            //    _view.DataGridViewDataSource.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;
            //}
            //catch (Exception ex)
            //{
            //    _messageService.ShowError(ex.Message);
            //}
            #endregion
        }

        private void _view_FileSelectExcelClick(object sender, EventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog();
            dlg.Filter = @"File Excel(*.xls;*.xlsx)|*.xls;*.xlsx|All files(*.*)|*.*";

            if (dlg.ShowDialog() == DialogResult.OK)
            {
                _view.FilePathExcel = dlg.FileName;
                Properties.Settings.Default.FilePathEXCEL = dlg.FileName;
                Properties.Settings.Default.Save();
            }
        }

        private void _view_FolderSelectPdfClick(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();

            if (fbd.ShowDialog()==DialogResult.OK)
            {
                _view.FolderPathPdf = fbd.SelectedPath;
                Properties.Settings.Default.FolderPathPDF = fbd.SelectedPath;
                Properties.Settings.Default.Save();
            }
        }
    }
}
