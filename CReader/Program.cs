﻿using System;
using System.Windows.Forms;
using CReader.BL;

namespace CReader
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            MainForm form = new MainForm();
            MessageService service = new MessageService();
            FileManager manager = new FileManager();

            MainPresenter preseter = new MainPresenter(form, manager, service);

            Application.Run(form);
        }
    }
}
